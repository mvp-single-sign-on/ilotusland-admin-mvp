import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import DefaultLayout from 'layouts/default'
// import { get as _get } from 'lodash-es'
import slug, { breadcrumb } from 'routes'

import SkeletonTab from 'components/skeleton/skeleton-tab/index.js'
import { Table } from 'antd'
import { getInfoErrorfetch } from 'api/fetch/index.js'
import dataOrganization from 'constants/dataSource/organization.json'

const PageWrapper = styled.div`
  display: flex;
  background: white;
  padding: 8px;
  flex-direction: column;
`

const mapStateToProps = () => ({})
const mapDispatchToProps = {}

const columns = [
  {
    title: 'Tên tổ chức',
    dataIndex: 'name',
    key: 'name',
    render: (text, record) => {
      return <Link href={`/orangization/${record.key}`}>{text}</Link>
    }
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
    key: 'address'
  }
]

@connect(mapStateToProps, mapDispatchToProps)
class HomePage extends React.Component {
  static propTypes = {
    updateSelectedMenu: PropTypes.func.isRequired,
    setBreadCrumb: PropTypes.func.isRequired,
    setPageName: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    // this.props.updateSelectedMenu([slug.home])
    this.props.setBreadCrumb(breadcrumb[slug.home])
    this.props.setPageName('Quản lý Tổ chức')
  }

  state = {
    isLoading: true,
    dataSource: [],
    pagination: {
      current: 1,
      pageSize: 5
    }
  }

  componentDidMount = async () => {
    try {
      // const result = await getConfig()
      this.setState({
        dataSource: dataOrganization
      })
    } catch (ex) {
      const { response } = ex
      getInfoErrorfetch(response)
    } finally {
      this.setState({
        isLoading: false
      })
    }
  }

  render() {
    // console.log(dataConfig, '----dataConfig--')
    return (
      <PageWrapper>
        <SkeletonTab loading={this.state.isLoading} />
        {!this.state.isLoading && (
          <div>
            <Table dataSource={this.state.dataSource} columns={columns} />
          </div>
        )}
      </PageWrapper>
    )
  }
}

HomePage.Layout = DefaultLayout
export default HomePage
