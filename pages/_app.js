import React from 'react'
import App, { Container } from 'next/app'
// import { PageTransition } from 'next-page-transitions'
// import { Icon } from 'antd'
// import dynamic from 'next/dynamic'
import Head from 'next/head'
import { Router } from 'next/router'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
// import { Spin, Icon } from 'antd'

import Loader from 'components/loading-screen'
import { getOrCreateStore } from 'lib/with-redux-store'

import '../src/less/app/index.less'
import '../src/less/waves.less'

// const Waves = dynamic(() => import('node-waves'), { ssr: false })

const reduxStore = getOrCreateStore()

class MyApp extends App {
  constructor(props) {
    super(props)
    this.reduxStore = reduxStore
    this.persistor = persistStore(reduxStore)

    this.state = {
      isLoadingPage: false
    }
  }

  componentDidMount() {
    if (!window.Waves) {
      window.Waves = require('node-waves')
      window.Waves.init()
    }

    Router.events.on('routeChangeStart', url => {
      console.log(`Loading: ${url}`)
      this.setState({ isLoadingPage: true })
    })
    Router.events.on('routeChangeComplete', () => {
      setTimeout(() => {
        this.setState({ isLoadingPage: false })
      }, 200)
    })
    Router.events.on('routeChangeError', () => this.setState({ isLoadingPage: false }))
  }

  render() {
    const { isLoadingPage } = this.state
    const { Component, pageProps, router } = this.props
    const Layout = Component.Layout

    return (
      <Container>
        <Head>
          <title>ilotusland admin</title>
        </Head>
        <Provider store={reduxStore}>
          <PersistGate loading={<Loader />} persistor={this.persistor}>
            <Layout isLoadingPage={isLoadingPage} keyPage={router.route}>
              {!isLoadingPage && <Component {...pageProps} />}
            </Layout>
          </PersistGate>
        </Provider>
      </Container>
    )
  }
}

export default MyApp
