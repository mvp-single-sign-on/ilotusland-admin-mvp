import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import DefaultLayout from 'layouts/default'
import slug, { breadcrumb } from 'routes'

const mapStateToProps = () => ({})
const mapDispatchToProps = {}

@connect(mapStateToProps, mapDispatchToProps)
class AboutPage extends React.Component {
  static propTypes = {
    updateSelectedMenu: PropTypes.func.isRequired,
    setBreadCrumb: PropTypes.func.isRequired,
    setPageName: PropTypes.func.isRequired
  }

  UNSAFE_componentWillMount() {
    this.props.updateSelectedMenu([slug.about])
    this.props.setBreadCrumb(breadcrumb[slug.about])
    this.props.setPageName('About')
  }
  render() {
    return (
      <div>
        <span>about page</span>
      </div>
    )
  }
}

AboutPage.Layout = DefaultLayout
export default AboutPage
