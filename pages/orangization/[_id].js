import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Avatar, Tabs, Table, Typography, Switch, Card, Row, Col } from 'antd'

import { connect } from 'react-redux'
import DefaultLayout from 'layouts/default'
import { find as _find } from 'lodash-es'
// import slug, { breadcrumb } from 'routes'
import dataOrganization from 'constants/dataSource/organization.json'
import dataUsers from 'constants/dataSource/users.json'
import fetch from 'node-fetch'
import { patchFetch } from 'utils/fetch'

const HOST_BACKEND = 'http://103.89.85.226:3001'
// const HOST_BACKEND = 'http://localhost:3001'

const { Meta } = Card
const { Text } = Typography
const { TabPane } = Tabs

const mapStateToProps = () => ({})
const mapDispatchToProps = {}

const PageWrapper = styled.div`
  display: flex;
  background: white;
  padding: 8px;
  flex-direction: column;

  .tab-main-app {
    .ant-card-meta {
      display: flex;
      align-items: center;
    }
  }
`

const columns = [
  {
    title: 'email',
    dataIndex: 'email',
    key: 'email',
    render: text => <Text strong>{text}</Text>
  },
  {
    title: 'Tên',
    dataIndex: 'name',
    key: 'name'
  }
]

// const CIcon = ({ path, ...rest }) => {
//   CIcon.propTypes = {
//     path: PropTypes.string
//   }
//   const ImportedIconRef = React.useRef(null)
//   const [loading, setLoading] = React.useState(false)

//   React.useEffect(() => {
//     setLoading(true)
//     const importIcon = async () => {
//       try {
//         ImportedIconRef.current = (await import(path)).ReactComponent
//       } catch (err) {
//         // Your own error handling logic, throwing error for the sake of
//         // simplicity
//         throw err
//       } finally {
//         setLoading(false)
//       }
//     }
//     importIcon()
//   }, [path])

//   if (!loading && ImportedIconRef.current) {
//     const { current: ImportedIcon } = ImportedIconRef
//     return <ImportedIcon {...rest} />
//   }

//   return null
// }

const App = ({ id, name, logo, isEnable, handleChange }) => {
  App.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    logo: PropTypes.string,
    isEnable: PropTypes.bool,
    handleChange: PropTypes.func
  }
  const handleToggleEnable = async isEnable => {
    console.log('==isEnable===>', isEnable)
    await handleChange({ appId: id, isEnable })
    // await patchFetch(`${HOST_BACKEND}/app/${id}`, { isEnable })
    // console.log(isEnable, '==isEnable....=>')
  }
  console.log({ id, name, logo, isEnable, handleChange })
  return (
    <Col span={6}>
      <Card
        style={{ marginTop: 16 }}
        actions={[
          <Switch
            key={1}
            defaultChecked={isEnable}
            onChange={handleToggleEnable}
            checkedChildren='ON '
            unCheckedChildren='OFF'
          />
        ]}
      >
        <Meta
          avatar={<Avatar style={{ fontSize: '3rem' }} src={`${HOST_BACKEND}/${logo}`} />}
          title={name}
          description='...'
        />
      </Card>
    </Col>
  )
}

const Apps = ({ dataSource, handleChange }) => {
  Apps.propTypes = {
    dataSource: PropTypes.array,
    handleChange: PropTypes.func
  }
  // return <div>main apo</div>
  return (
    <div className='tab-main-app'>
      <Row gutter={[8, 16]}>
        {dataSource.map((app, index) => {
          return (
            <App
              key={index}
              handleChange={handleChange}
              id={app.id}
              name={app.name}
              logo={app.logo}
              isEnable={app.isEnable}
            />
          )
        })}
      </Row>
    </div>
  )
}

@connect(mapStateToProps, mapDispatchToProps)
class ListStationPage extends React.Component {
  static propTypes = {
    _id: PropTypes.string,
    updateSelectedMenu: PropTypes.func.isRequired,
    setBreadCrumb: PropTypes.func.isRequired,
    setPageName: PropTypes.func.isRequired,
    apps: PropTypes.array.isRequired,
    addons: PropTypes.array.isRequired
  }
  static getInitialProps = async ({ query }) => {
    console.log('==getInitialProps===')
    const { _id } = query
    const appResponse = await fetch(`${HOST_BACKEND}/app`)
    // console.log(appResponse, '===appResponse===>')
    const apps = await appResponse.json()
    console.log(apps, '====apps===>')
    const addonResponse = await fetch(`${HOST_BACKEND}/addon`)
    const addons = await addonResponse.json()
    return {
      _id,
      apps,
      addons
    }
  }
  constructor(props) {
    super(props)
    // this.props.updateSelectedMenu([slug.home])
    // this.props.setBreadCrumb(breadcrumb[slug.home])
    // this.props.setPageName('Tổ chức A')
  }

  componentDidMount = () => {
    const record = _find(dataOrganization, item => {
      return item.key === this.props._id
    })
    this.props.setPageName(record.name)
    console.log(this.props.apps, '=app===> ')
  }

  render() {
    console.log(dataUsers, '--dataUsers--')
    return (
      <PageWrapper>
        <Tabs defaultActiveKey='2'>
          <TabPane tab='Tài khoản' key='1'>
            <div>{dataUsers && <Table dataSource={dataUsers} columns={columns} />}</div>
          </TabPane>
          <TabPane tab='Ứng dụng chính' key='2'>
            <Apps
              dataSource={this.props.apps}
              handleChange={async ({ appId, isEnable }) => {
                await patchFetch(`${HOST_BACKEND}/app/${appId}`, { isEnable })
              }}
            />
          </TabPane>
          <TabPane tab='Add on' key='3'>
            <Apps
              dataSource={this.props.addons}
              handleChange={async ({ appId, isEnable }) => {
                await patchFetch(`${HOST_BACKEND}/addon/${appId}`, { isEnable })
              }}
            />
          </TabPane>
          <TabPane tab='Market place' key='4'>
            Content of Tab Pane 4
          </TabPane>
        </Tabs>
      </PageWrapper>
    )
  }
}

ListStationPage.Layout = DefaultLayout
export default ListStationPage
