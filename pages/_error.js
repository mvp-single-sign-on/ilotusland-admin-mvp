import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Clearfix from 'components/elements/clearfix'
import EmptyLayout from 'layouts/empty'
import Router from 'next/router'
import slug from 'routes'
import { connect } from 'react-redux'
import { updateSelectedMenu } from '@redux/actions/generalAction'

import { Button } from 'antd'

const ErrorWrapper = styled.div`
  flex: 1;
  min-width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`
@connect(() => ({}), { updateSelectedMenu })
class Error extends React.Component {
  static propTypes = {
    updateSelectedMenu: PropTypes.func
  }
  render() {
    return (
      <ErrorWrapper>
        <div>
          <img src='/static/assets/images/404.svg/' />
        </div>
        {/* <Clearfix height={8} /> */}
        <div>
          <h2>404</h2>
        </div>
        <div>
          <span> Sorry, the page you visited does not exist.</span>
        </div>
        <Clearfix height={8} />
        <div>
          <Button
            type='primary'
            onClick={() => {
              this.props.updateSelectedMenu(keyPath)
              Router.push(slug.home)
            }}
          >
            Về trang chủ
          </Button>
        </div>
      </ErrorWrapper>
    )
  }
}
Error.Layout = EmptyLayout
export default Error
