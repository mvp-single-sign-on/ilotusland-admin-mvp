const jsonServer = require('json-server')
const server = jsonServer.create()
const path = require('path')
var express = require('express')

const router = jsonServer.router(path.join(__dirname, 'db.json'))
// const router = jsonServer.router('./db.json')
const middlewares = jsonServer.defaults()
const cors = require('cors')

server.use('/media', express.static(path.join(__dirname, 'public')))

server.use(
  cors({
    origin: true,
    credentials: true,
    preflightContinue: false,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
  })
)

// server.configure(function(){
//   server.use('/media', express.static(__dirname + '/media'));
//   server.use(express.static(__dirname + '/public'));
// });
server.options('*', cors())

server.use(middlewares)
server.use(router)
server.listen(3001, () => {
  console.log('JSON Server is running on 3001')
})
