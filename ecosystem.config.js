module.exports = {
  apps: [
    {
      name: 'AirLotus-Web-Admin',
      // script: 'next build & node server.js',
      script: 'yarn run build && node server.js',
      watch: true,
      env: {
        NODE_ENV: 'production',
        PORT: 3400,
        HOST_API: 'http://prtr.southeastasia.cloudapp.azure.com:3405',
        HOST_MEDIA: 'https://media.ilotusland.com',
        ORGANIZATION_KEY: 'HN'
      }
    }
  ]
}
