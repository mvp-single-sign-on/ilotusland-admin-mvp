/* eslint-disable */
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
const fs = require('fs')
const path = require('path')
const withPlugins = require('next-compose-plugins')

const withLess = require('@zeit/next-less')
const lessToJS = require('less-vars-to-js')
const withTM = require('next-transpile-modules')(['lodash-es'])

const CompressionPlugin = require('compression-webpack-plugin')
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

// Where your antd-custom.less file lives
const themeVariables = lessToJS(fs.readFileSync(path.resolve(__dirname, './src/less/antd-custom.less'), 'utf8'))

const nextConfig = {
  webpack: (config, { isServer, dev }) => {
    config.resolve.alias['@redux'] = path.join(__dirname, 'src/@redux')
    config.resolve.alias['api'] = path.join(__dirname, 'src/api')
    config.resolve.alias['components'] = path.join(__dirname, 'src/components')
    config.resolve.alias['containers'] = path.join(__dirname, 'src/containers')
    config.resolve.alias['constants'] = path.join(__dirname, 'src/constants')
    config.resolve.alias['hoc'] = path.join(__dirname, 'src/hoc')
    config.resolve.alias['layouts'] = path.join(__dirname, 'src/layouts')
    config.resolve.alias['lib'] = path.join(__dirname, 'src/lib')
    config.resolve.alias['routes'] = path.join(__dirname, 'src/routes')
    config.resolve.alias['utils'] = path.join(__dirname, 'src/utils')
    config.resolve.alias['static'] = path.join(__dirname, 'static')
    
    if (dev) {
      config.module.rules.push({
        test: /\.js$/,
        exclude: ['/node_modules/', '/.next/'],
        loader: 'eslint-loader',
        options: {
          emitWarning: true
        }
      })
    }
    config.module.rules.push({
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      use: [
        {
          loader: 'babel-loader'
        },
        {
          loader: '@svgr/webpack',
          options: {
            babel: false,
            icon: true
          }
        }
      ]
    })
    if (isServer) {
      const antStyles = /antd\/.*?\/style.*?/
      const origExternals = [...config.externals]
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback()
          if (typeof origExternals[0] === 'function') {
            origExternals[0](context, request, callback)
          } else {
            callback()
          }
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals)
      ]

      config.module.rules.unshift({
        test: antStyles,
        use: 'null-loader'
      })
    }

    if (!isServer && !dev) {
      config.optimization.splitChunks.cacheGroups.commons.minChunks = 3
    }

    if (config.mode === 'production') {
      if (Array.isArray(config.optimization.minimizer)) {
        config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin({}))
      }
    }
    config.plugins.push(
      new FilterWarningsPlugin({
        exclude: /mini-css-extract-plugin[^]*Conflicting order between:/
      })
    )
    config.plugins.push(new CompressionPlugin())

    return config
  },
  // env: {
  //   // Reference a variable that was defined in the .env file and make it available at Build Time
  //   HOST_API: process.env.HOST_API || 'http://localhost:3405',
  //   HOST_MEDIA:process.env.HOST_MEDIA || 'https://media.ilotusland.com',
  //   ORGANIZATION_KEY:process.env.ORGANIZATION_KEY || 'HN'
  // },

  // serverRuntimeConfig: {
  //   // Will only be available on the server side
  //   // mySecret: process.env.HOST_API+ 'secret'
  // },
  publicRuntimeConfig: {
    HOST_API: process.env.HOST_API || 'http://localhost:3405',
    HOST_MEDIA: process.env.HOST_MEDIA || 'https://media.ilotusland.com',
    ORGANIZATION_KEY: process.env.ORGANIZATION_KEY || 'HN'
  }
}

module.exports = withPlugins(
  [
    [
      withLess,
      {
        lessLoaderOptions: {
          javascriptEnabled: true,
          modifyVars: themeVariables, // make your antd custom effective
          ignoreOrder: true
        }
      }
    ],
    [withTM]
  ],
  nextConfig
)
