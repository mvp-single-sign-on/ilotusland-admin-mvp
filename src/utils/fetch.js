import axios from 'axios'
const getHeaders = () => {
  var headers = {
    Accept: 'application/json',
    [`Access-Control-Allow-Origin`]: 'http://localhost:3001'
  }

  return headers
}


export function putFetch(url, data, props) {
  let attributes = Object.assign(
    {
      cache: true,
      headers: getHeaders(),
    },
    props
  )

  return new Promise((resolve, reject) => {
    axios
      .put(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({ error: true })
        }
      })
      .catch(e => reject(e))
  })
}

export function patchFetch(url, data, props) {
  let attributes = Object.assign(
    {
      cache: true,
      headers: getHeaders(),
    },
    props
  )

  return new Promise((resolve, reject) => {
    axios
      .patch(url, data, attributes)
      .then(res => {
        if (res.status === 200) {
          resolve(res.data)
        } else {
          reject({ error: true })
        }
      })
      .catch(e => reject(e))
  })
}
