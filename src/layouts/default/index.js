import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  Layout,
  Menu,
  // Breadcrumb,
  Avatar,
  Spin
} from 'antd'
import { HomeOutlined, LoadingOutlined } from '@ant-design/icons'
// import hocProtectLogin from 'hoc/is-authenticated'
import { PageTransition } from 'next-page-transitions'
import { connect } from 'react-redux'
import { updateSelectedMenu, updateSubMenu, setBreadCrumb } from '@redux/actions/generalAction'
import { get as _get } from 'lodash-es'

import { withRouter } from 'next/router'
import windowSize from 'react-window-size'
import AvatarUser from './avatar-user'
import Router from 'next/router'
import slug from 'routes/index'
import moment from 'moment'

// const RippleMenuCom = () => <div style={{ position: 'absolute', width: '100%', height: '100%', top: 0, left: 0 }} />
const LoaderPageContainer = () => (
  <div style={{ top: '30%', left: '55%', position: 'absolute' }}>
    <Spin indicator={<LoadingOutlined style={{ fontSize: 32 }} />} />
  </div>
)

const { Header, Content, Footer, Sider } = Layout
// const { SubMenu } = Menu

const LayoutWrapper = styled.div`
  display: flex;
  flex: 1;

  .sider-menu-logo {
    position: relative;
    height: 64px;
    padding-left: 24px;
    overflow: hidden;
    line-height: 64px;
    background: #001529;
    cursor: pointer;
    transition: all 0.3s;
    display: flex;
    align-items: center;

    h1 {
      color: white;
      margin: 0px;
      padding-left: 8px;
    }
  }

  .header {
    display: flex;
    justify-content: space-between;
    background: #fff;
    padding: 0;
    box-shadow: 0 1px 4px rgba(0, 21, 41, 0.08);

    .header--left {
      padding-left: 24px;
    }
    .header--right {
      display: flex;
      align-items: center;
      .ant-menu-horizontal {
        border-bottom: none;
      }
      .ant-menu-item ant-menu-item-selected {
        border-bottom: none;
      }
    }
  }
`
const ChildrenContainer = styled.div``

@connect(
  state => ({
    isAuthenticated: _get(state, 'AuthStore.isAuthenticated', false),
    token: _get(state, 'AuthStore.token', null),
    subMenu: _get(state, 'GeneralStore.menu.subMenu', []),
    selectedMenu: _get(state, 'GeneralStore.menu.selectedMenu', []),
    breadcrumb: _get(state, 'GeneralStore.menu.breadcrumb', [])
  }),
  { updateSelectedMenu, updateSubMenu, setBreadCrumb }
)
// @hocProtectLogin // MARK tạm thời bỏ
@windowSize
class AppWithLayout extends React.Component {
  static propTypes = {
    breadcrumb: PropTypes.array,
    children: PropTypes.node,
    isAuthenticated: PropTypes.bool,
    isLoadingPage: PropTypes.bool,
    keyPage: PropTypes.string.isRequired,
    selectedMenu: PropTypes.array,
    subMenu: PropTypes.array,
    token: PropTypes.string,
    updateSelectedMenu: PropTypes.func,
    updateSubMenu: PropTypes.func,
    setBreadCrumb: PropTypes.func,
    windowWidth: PropTypes.number
  }

  state = {
    isLoaded: false,
    collapsed: false,
    broken: false,
    pageName: ''
  }

  onCollapse = collapsed => {
    // console.log(collapsed)
    this.setState({ collapsed })
  }
  componentDidMount = async () => {
    // MARK  waves effect
    window.Waves.attach('li.ant-menu-item div')
    // const { isAuthenticated } = this.props
    // if (!isAuthenticated) {
    //   Router.push(slug.login)
    // }
  }

  hanldeOnSelect = ({ keyPath }) => {
    console.log('keyPath', keyPath)

    this.props.updateSelectedMenu(keyPath)
    Router.push(keyPath[0])
  }

  handleOnOpenChange = openKeys => {
    this.props.updateSubMenu(openKeys)
  }

  handleChangePageName = pageName => this.setState({ pageName })

  render() {
    const { children, windowWidth, isAuthenticated, subMenu, selectedMenu } = this.props
    return (
      <LayoutWrapper windowWidth={windowWidth}>
        <Layout>
          <Sider
            breakpoint='lg'
            width={250}
            collapsedWidth={this.state.broken ? '0' : '80'}
            onBreakpoint={broken => {
              this.setState({ broken })
            }}
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <div className='sider-menu-logo'>
              {/* <Icon style={{ fontSize: '2rem' }} component={pathLogo.logo} /> */}
              <Avatar src={'/static/assets/images/logo.svg'} size={40} />
              {!this.state.collapsed && <h1>iLotusland</h1>}
            </div>
            <Menu
              theme='dark'
              openKeys={subMenu}
              selectedKeys={selectedMenu}
              mode='inline'
              onSelect={this.hanldeOnSelect}
              onOpenChange={this.handleOnOpenChange}
            >
              <Menu.Item key={slug.home} style={{ position: 'relative' }}>
                <HomeOutlined />
                <span>Tổ chức</span>
              </Menu.Item>
              <Menu.Item key={slug.marketPlace} style={{ position: 'relative' }}>
                <HomeOutlined />
                <span>Market place</span>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Header className='header' style={{}}>
              <div className='header--left'>
                <h2>{this.state.pageName}</h2>
              </div>
              <div className='header--right'>
                {!isAuthenticated && (
                  <div>
                    <AvatarUser />
                  </div>
                )}
              </div>
            </Header>
            <Content style={{ margin: '16px 16px' }}>
              <div style={{ minHeight: 360 }}>
                {this.props.isLoadingPage ? (
                  <LoaderPageContainer />
                ) : (
                  <PageTransition timeout={200} classNames='page-transition'>
                    <ChildrenContainer key={this.props.keyPage}>
                      {React.Children.map(children, child => {
                        return React.cloneElement(child, {
                          updateSelectedMenu: this.props.updateSelectedMenu,
                          setBreadCrumb: this.props.setBreadCrumb,
                          setPageName: this.handleChangePageName
                        })
                      })}
                    </ChildrenContainer>
                  </PageTransition>
                )}
              </div>
              <style jsx global>{`
                .page-transition-enter {
                  opacity: 0;
                }
                .page-transition-enter-active {
                  opacity: 1;
                  transition: opacity 300ms;
                }
                .page-transition-exit {
                  opacity: 1;
                }
                .page-transition-exit-active {
                  opacity: 0;
                  transition: opacity 300ms;
                }
              `}</style>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Project ©{moment().format('YYYY')} Created by VAS</Footer>
          </Layout>
        </Layout>
      </LayoutWrapper>
    )
  }
}

export default withRouter(AppWithLayout)
