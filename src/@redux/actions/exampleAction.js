export const CHANGES = 'EXAMPLE/CHANGES'

export function exampleAction() {
  return dispatch => {
    dispatch({
      type: CHANGES,
      payload: null
    })
  }
}
