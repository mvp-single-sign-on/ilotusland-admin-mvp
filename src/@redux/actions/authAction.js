export const LOGIN = 'AUTH/LOGIN'
export const LOGOUT = 'AUTH/LOGOUT'

// ACTIONS
export function userLogin(token, userInfo) {
  return dispatch => {
    dispatch({
      type: LOGIN,
      payload: {
        token,
        userInfo
      }
    })
  }
}

export function userLogout() {
  return dispatch => {
    dispatch({ type: LOGOUT })
  }
}
