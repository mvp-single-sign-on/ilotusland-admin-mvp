// NOTE  Menu
export const UPDATE_GENERAL_SUB_MENU = 'GENERAL/UPDATE_GENERAL_SUB_MENU'
export const UPDATE_GENERAL_SELECTED_MENU = 'GENERAL/UPDATE_GENERAL_SELECTED_MENU'
export const UPDATE_GENERAL_SET_BREADCRUMB = 'GENERAL/UPDATE_GENERAL_SET_BREADCRUMB'

// NOTE  Menu
export function updateSubMenu(data) {
  return async dispatch => {
    dispatch({ type: UPDATE_GENERAL_SUB_MENU, payload: data })
  }
}

export function updateSelectedMenu(data) {
  return async dispatch => {
    dispatch({ type: UPDATE_GENERAL_SELECTED_MENU, payload: data })
  }
}

export function setBreadCrumb(breadcrumbArr) {
  return dispatch => {
    const payload = Array.isArray(breadcrumbArr) ? breadcrumbArr : []
    dispatch({ type: UPDATE_GENERAL_SET_BREADCRUMB, payload })
  }
}
