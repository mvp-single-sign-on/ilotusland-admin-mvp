import produce from 'immer'
import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

import {
  UPDATE_GENERAL_SUB_MENU,
  UPDATE_GENERAL_SELECTED_MENU,
  UPDATE_GENERAL_SET_BREADCRUMB
} from '../actions/generalAction'

const InitialState = {
  menu: {
    subMenu: [],
    selectedMenu: [],
    breadcrumb: []
  }
}

// REDUCERS
const generalReducer = produce((draft = InitialState, action) => {
  switch (action.type) {
    case UPDATE_GENERAL_SUB_MENU: {
      draft.menu.subMenu = action.payload
      return
    }
    case UPDATE_GENERAL_SELECTED_MENU: {
      draft.menu.selectedMenu = action.payload
      return
    }
    case UPDATE_GENERAL_SET_BREADCRUMB: {
      draft.menu.breadcrumb = action.payload
      return
    }
    default:
      return draft
  }
})

const generalPersistConfig = {
  key: 'GeneralStore',
  storage: storage,
  blacklist: ['danhMuc']
}

export default persistReducer(generalPersistConfig, generalReducer)
