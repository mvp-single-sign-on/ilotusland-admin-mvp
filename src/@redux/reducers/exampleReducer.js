import produce from 'immer'
import { CHANGES } from '../actions/exampleAction'

const InitialState = {
  tamp: null
}

// REDUCERS
const exampleReducer = produce((draft = InitialState, action) => {
  switch (action.type) {
    case CHANGES: {
      if (!draft.tamp) draft.tamp = 1
      else draft.tamp++
      return
    }
    default:
      return draft
  }
})

// const examplePersistConfig = {
//   key: 'ExampleStore',
//   storage: storage, import storage from 'redux-persist/lib/storage'
//   blacklist: ['key_black_list']
// }

// export default persistReducer(examplePersistConfig, exampleReducer) // import { persistReducer } from 'redux-persist'

export default exampleReducer
