import { combineReducers } from 'redux'
import exampleReducer from './exampleReducer'
import authReducer from './authReducer'
import generalReducer from './generalReducer'

export default combineReducers({
  ExampleStore: exampleReducer,
  AuthStore: authReducer,
  GeneralStore: generalReducer
})
