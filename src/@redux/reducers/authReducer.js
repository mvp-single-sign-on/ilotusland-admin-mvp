import produce from 'immer'
import { LOGIN, LOGOUT } from '../actions/authAction'

const InitialState = {
  token: null,
  isAuthenticated: false,
  userInfo: null
}

// REDUCERS
const authReducer = produce((draft = InitialState, action) => {
  switch (action.type) {
    case LOGIN: {
      const { token, userInfo } = action.payload
      draft.isAuthenticated = true
      draft.token = token
      draft.userInfo = userInfo
      return
    }
    case LOGOUT: {
      draft.isAuthenticated = false
      draft.token = null
      draft.userInfo = null
      return
    }
    default:
      return draft
  }
})

export default authReducer
