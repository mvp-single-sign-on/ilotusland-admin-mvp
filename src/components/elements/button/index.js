import React from 'react'
import PropTypes from 'prop-types'
// import styled from 'styled-components'
import { Button } from 'antd'

const RippleMenuCom = () => <div style={{ position: 'absolute', width: '100%', height: '100%', top: 0, left: 0 }} />
export default class ButtonElement extends React.Component {
  static propTypes = {
    ...Button.propTypes,
    children: PropTypes.any,
    type: PropTypes.string,
    icon: PropTypes.string,
    size: PropTypes.string
  }
  Comp = React.createRef()
  componentDidMount() {
    const domButton = this.Comp.current.buttonNode
    window.Waves.attach(domButton.querySelector('div'))
  }
  render() {
    return (
      <Button {...this.props} ref={this.Comp}>
        {this.props.children}
        <RippleMenuCom />
      </Button>
    )
  }
}
