import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Skeleton, Row, Col } from 'antd'

const ComponentWrapper = styled.div`
  display: flex;
  background: white;
  padding: 8px;
  flex-direction: column;
`

const SkeletonTab = ({ loading }) => {
  // You can use Hooks here!
  return (
    <ComponentWrapper>
      {loading && (
        <Row gutter={8}>
          <Col>
            <Skeleton.Button active />
          </Col>
          <Col>
            <Skeleton.Button active />
          </Col>
          <Col>
            <Skeleton.Button active />
          </Col>
        </Row>
      )}

      <Row>
        <Skeleton loading={loading} active paragraph={{ rows: 7 }} />
      </Row>
    </ComponentWrapper>
  )
}

SkeletonTab.propTypes = {
  loading: PropTypes.bool
}

export default SkeletonTab
