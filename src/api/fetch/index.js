import axios from 'axios'
import { message as messageAnt, Modal } from 'antd'
import { get as _get } from 'lodash-es'
import { getOrCreateStore } from 'lib/with-redux-store'
import { userLogout } from '@redux/actions/authAction'
import slug from 'routes/index'
import Router from 'next/router'

import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()
const { HOST_API, ORGANIZATION_KEY } = publicRuntimeConfig

const reduxStore = getOrCreateStore()

const fetch = axios.create({
  baseURL: HOST_API,
  timeout: 30000,
  headers: { OrganizationKey: ORGANIZATION_KEY }
})

// Add a response interceptor
fetch.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    console.error('error fetch', error)
    if (error.response) {
      const { data } = error.response
      // console.log('codeName', data.codeName)
      if (data.codeName === 'AuthForceLogout') {
        reduxStore.dispatch(userLogout())
        Router.push(slug.login)
      }

      messageAnt.error(data.message)
    }
    return Promise.reject(error)
  }
)

export function setAuthorizationforHeader(token) {
  // console.log(token, 'token')
  fetch.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export function getInfoErrorfetch(response) {
  if (!response) {
    return
  }
  console.log('catch', response)
  const { status, data } = response
  switch (status) {
    case 401: {
      messageAnt.error(_get(data, 'messageDisplay', 'something wrong'))
      break
    }
    case 400: {
      Modal.error({
        title: 'Thất bại',
        content: _get(data, 'messageDisplay', 'something wrong')
      })
      break
    }
    case 503: {
      Modal.error({
        title: 'Thông báo server',
        content: 'Máy chủ quá “bận” hoặc trang web đang trong quá trình bảo trì'
      })
      break
    }
    case 409: {
      console.log('Error', data)
      break
    }
    default: {
      break
    }
  }
}

export default fetch
