import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig()
const { HOST_MEDIA } = publicRuntimeConfig

import pathParse from 'path-parse'
import { startsWith as _startsWith } from 'lodash-es'

const SLUG = `${HOST_MEDIA}/`
export function getFilePrivate(src, token) {
  const URL = `${SLUG}${src}?token=${token}`
  // console.log("getFilePrivate",token)
  const result = pathParse(src)

  return {
    URL,
    ...result,
    ext: result.ext.replace('.', '')
  }
}

export function getFilePublic(src) {
  if (_startsWith(src, 'http')) {
    return src
  }

  const URL = `${SLUG}${src.replace('public/', '')}`
  return URL
}

export default {
  getFilePrivate,
  getFilePublic
}
