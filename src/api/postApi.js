import fetch from './fetch'

const SLUG_STATION = '/post/w'

export function getList({ current = 1, pageSize = 100, ...querySearch }) {
  return fetch.get(`${SLUG_STATION}`, {
    params: {
      page: current,
      itemPerPage: pageSize,
      ...querySearch
    }
  })
}

export function getConfig() {
  return fetch.get(`${SLUG_STATION}/config`)
}

export function configPost({ isAutoApproved }) {
  return fetch.put(`${SLUG_STATION}/config`, {
    isAutoApproved,
    user: {
      name: 'Admin'
    }
  })
}

export function verifyPost({ postStatus, postIds }) {
  return fetch.put(`${SLUG_STATION}/verify`, {
    postStatus,
    postIds,
    user: {
      name: 'Admin'
    }
  })
}

export default {
  getList,
  configPost,
  verifyPost
}
