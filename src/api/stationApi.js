import fetch from './fetch'

const SLUG_STATION = '/station'

export function getList({ current = 1, pageSize = 100, ...querySearch }) {
  return fetch.get(`${SLUG_STATION}`, {
    params: {
      page: current,
      itemPerPage: pageSize,
      ...querySearch
    }
  })
}

export default {
  getList
}
