const slug = {
  home: '/',
  marketPlace: '/market-place',
  login: '/auth/login'
}

// NOTE  cấu trúc item breadcrumb là 1 array object
// + object gồm: slug và name
// last item sẽ k0 có slug
export const breadcrumb = {
  [slug.home]: [{ name: 'Quản lý Tổ chức' }]
}

export default slug
