export function tranferPagination({ page, itemPerPage, totalItem }) {
  return {
    page: page,
    pageSize: itemPerPage,
    current: page,
    total: totalItem
  }
}
