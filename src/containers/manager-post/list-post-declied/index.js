import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'
import { Table, Avatar, Typography, Button } from 'antd'
import { UserOutlined } from '@ant-design/icons'

import TimeAgo from 'react-timeago'
import viStrings from 'react-timeago/lib/language-strings/vi'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'
import { get as _get, omit as _omit, replace as _replace } from 'lodash-es'

import { getList } from 'api/postApi.js'
import { tranferPagination } from 'constants/funcGenaral.js'
import { getInfoErrorfetch } from 'api/fetch/index.js'
import { getFilePublic } from 'api/updateFileApi.js'
const { Text } = Typography

const formatter = buildFormatter(viStrings)

const ContainerWrapper = styled.div`
  display: flex;
`
const TIMER = 1000

class ListPostDelied extends React.Component {
  static propTypes = {
    onRowSelection: PropTypes.func,
    status: PropTypes.string.isRequired,
    onClickVerifedPost: PropTypes.func.isRequired,
    onClickDeclinedPost: PropTypes.func.isRequired,
    loading: PropTypes.bool
  }

  state = {
    isLoading: true,
    dataSource: [],
    pagination: {
      current: 1,
      pageSize: 20
    },
    selectedRowKeys: []
  }

  constructor(props) {
    super(props)
    this.getDataSource()
  }

  getDataSource = async () => {
    // console.log(this.state.pagination)
    const timer = setTimeout(async () => {
      try {
        const params = {
          ..._omit(this.state.pagination, ['total']),
          postStatus: this.props.status
        }
        const res = await getList(params)
        if (res.status === 200) {
          this.setState({
            dataSource: _get(res, 'data.list', []),
            pagination: tranferPagination(_get(res, 'data.pagination', {})),
            isLoading: false
          })
        }
      } catch (ex) {
        const { response } = ex
        // console.log('catch', response)
        getInfoErrorfetch(response)
      } finally {
        this.setState({
          isLoading: false
        })
      }

      clearTimeout(timer)
    }, TIMER)
  }

  getColumn = () => {
    return [
      {
        title: 'Hình ảnh',
        dataIndex: 'attachments',
        width: 100,
        render: value => {
          const images = _get(value, 'images', [])
          return <div>{images.length > 0 && <Avatar size={64} shape='square' src={getFilePublic(images[0])} />}</div>
        }
      },
      {
        title: 'Nội dung bài viết',
        key: '2',
        dataIndex: 'content',
        width: 300
      },
      {
        key: '3',
        title: 'Người đăng',
        dataIndex: 'actor',
        width: 300,
        render: value => {
          const phone = _get(value, 'phone')
          const picture = _get(value, 'picture', null)
          // console.log(picture, '----picture---')
          return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div>{picture && <Avatar src={getFilePublic(_get(value, 'picture'))} />}</div>
              <div>{!picture && <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />}</div>

              <div style={{ paddingLeft: '8px' }}>
                <div>{_get(value, 'name')}</div>
                <div>
                  <Text type='secondary' style={{ fontSize: '0.8333rem' }}>
                    {_replace(phone, '+84', 0)}
                  </Text>
                </div>
              </div>
            </div>
          )
        }
      },
      {
        key: '4',
        title: 'Ngày tạo',
        dataIndex: 'createAt',
        render: value => {
          return (
            <div>
              <TimeAgo formatter={formatter} date={value} />
            </div>
          )
        }
      },
      {
        key: '5',
        title: 'Hành động',
        dataIndex: '_id',
        render: value => {
          return (
            <div>
              <Button
                type='link'
                onClick={() => {
                  this.props.onClickVerifedPost([value])
                }}
              >
                Duyệt
              </Button>
            </div>
          )
        }
      }
    ]
  }

  handleTableChange = pagination => {
    let pager = { ...this.state.pagination }

    pager.page = pagination.current
    this.setState(
      {
        pagination: pager
      },
      () => {
        this.getDataSource()
      }
    )
  }
  componentDidUpdate = () => {
    // console.log('---getDerivedStateFromProps--')
    if (this.props.loading === true && this.props.loading !== this.state.isLoading) {
      // console.log( "componentWillReceiveProps" )
      this.setState({
        isLoading: true,
        selectedRowKeys: []
      })
      this.getDataSource()
    }
  }

  render() {
    // console.log(this.state.dataSource, '-----')
    return (
      <ContainerWrapper>
        <Table
          rowKey='_id'
          rowSelection={{
            type: 'checkbox',
            selectedRowKeys: this.state.selectedRowKeys,
            onChange: selectedRowKeys => {
              // console.log(selectedRowKeys, '----')
              this.setState({
                selectedRowKeys: selectedRowKeys
              })
              if (this.props.onRowSelection) {
                this.props.onRowSelection(selectedRowKeys)
              }
            }
          }}
          loading={this.state.isLoading}
          columns={this.getColumn()}
          dataSource={this.state.dataSource}
          pagination={this.state.pagination}
          onChange={this.handleTableChange}
        />
      </ContainerWrapper>
    )
  }
}

export default ListPostDelied
