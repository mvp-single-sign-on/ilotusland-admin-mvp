FROM node:10.15.1

WORKDIR /app
COPY . .

ENV NODE_ENV production
ENV HOST_API https://airlotus-api.ilotusland.com
ENV HOST_MEDIA https://media.ilotusland.com
ENV ORGANIZATION_KEY HN

RUN npm install
RUN npm run build

EXPOSE 8010
CMD node server.js